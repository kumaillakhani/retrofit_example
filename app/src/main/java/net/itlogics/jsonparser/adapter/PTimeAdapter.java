package net.itlogics.jsonparser.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.itlogics.jsonparser.R;
import net.itlogics.jsonparser.model.PTime;

import java.util.ArrayList;
import java.util.List;

import static net.itlogics.jsonparser.R.id.tvAsr;
import static net.itlogics.jsonparser.R.id.tvDateFor;
import static net.itlogics.jsonparser.R.id.tvDhuhr;
import static net.itlogics.jsonparser.R.id.tvFajr;
import static net.itlogics.jsonparser.R.id.tvIsha;
import static net.itlogics.jsonparser.R.id.tvMaghrib;
import static net.itlogics.jsonparser.R.id.tvShurooq;

/**
 * Created by Kumail Raza on 9/4/2016.
 */

public class PTimeAdapter extends ArrayAdapter<PTime> {

//    TextView tvDateFor, tvFajr, tvShurooq, tvDhuhr, tvAsr, tvMaghrib, tvIsha;
    List<PTime> pTimeList;
    Context context;
    private LayoutInflater mInflater;


    public PTimeAdapter(Context context, List<PTime> objects) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        pTimeList = objects;
    }

    @Override
    public PTime getItem(int position) {
        return pTimeList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.item_ptime, parent, false);
            vh = ViewHolder.create((LinearLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        PTime item = getItem(position);

//        vh.textViewName.setText(item.getName());
//        vh.textViewEmail.setText(item.getEmail());
//        Picasso.with(context).load(item.getProfilePic()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);

        vh.tvDateFor.setText(item.getDate_for());
        vh.tvFajr.setText(item.getFajr());
        vh.tvShurooq.setText(item.getShurooq());
        vh.tvDhuhr.setText(item.getDhuhr());
        vh.tvAsr.setText(item.getAsr());
        vh.tvMaghrib.setText(item.getMaghrib());
        vh.tvIsha.setText(item.getIsha());

        return vh.rootView;
    }

    private static class ViewHolder {
        public final LinearLayout rootView;
//        public final ImageView imageView;
//        public final TextView textViewName;
        public final TextView tvDateFor, tvFajr, tvShurooq, tvDhuhr, tvAsr, tvMaghrib, tvIsha;

        private ViewHolder(LinearLayout rootView, TextView tvDateFor, TextView tvFajr,
                           TextView tvShurooq, TextView tvDhuhr, TextView tvAsr, TextView tvMaghrib,
                           TextView tvIsha) {
            this.rootView = rootView;
            this.tvDateFor = tvDateFor;
            this.tvFajr = tvFajr;
            this.tvShurooq = tvShurooq;
            this.tvDhuhr = tvDhuhr;
            this.tvAsr = tvAsr;
            this.tvMaghrib = tvMaghrib;
            this.tvIsha = tvIsha;
        }

        public static ViewHolder create(LinearLayout rootView) {
//            TextView textViewName = (TextView) rootView.findViewById(R.id.textViewName);
//            TextView textViewEmail = (TextView) rootView.findViewById(R.id.textViewEmail);
            TextView tvDateFor = (TextView) rootView.findViewById(R.id.tvDateFor);
            TextView tvFajr = (TextView) rootView.findViewById(R.id.tvFajr);
            TextView tvShurooq = (TextView) rootView.findViewById(R.id.tvShurooq);
            TextView tvDhuhr = (TextView) rootView.findViewById(R.id.tvDhuhr);
            TextView tvAsr = (TextView) rootView.findViewById(R.id.tvAsr);
            TextView tvMaghrib = (TextView) rootView.findViewById(R.id.tvMaghrib);
            TextView tvIsha = (TextView) rootView.findViewById(R.id.tvIsha);

            return new ViewHolder(rootView, tvDateFor, tvFajr, tvShurooq, tvDhuhr, tvAsr, tvMaghrib,
                    tvIsha);
        }
    }





//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        // Get the data item for this position
//        PTime pTime = getItem(position);
//        // Check if an existing view is being reused, otherwise inflate the view
//        if (convertView == null) {
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_ptime, parent, false);
//        }
//        // Lookup view for data population
//        tvDateFor = (TextView) convertView.findViewById(tvDateFor);
//        tvFajr = (TextView) convertView.findViewById(tvFajr);
//        tvShurooq = (TextView) convertView.findViewById(tvShurooq);
//        tvDhuhr = (TextView) convertView.findViewById(tvDhuhr);
//        tvAsr = (TextView) convertView.findViewById(tvAsr);
//        tvMaghrib = (TextView) convertView.findViewById(tvMaghrib);
//        tvIsha = (TextView) convertView.findViewById(tvIsha);
//
//        // Populate the data into the template view using the data object
//        tvDateFor.setText(pTime.getDate_for());
//        tvFajr.setText(pTime.getFajr());
//        tvShurooq.setText(pTime.getShurooq());
//        tvDhuhr.setText(pTime.getDhuhr());
//        tvAsr.setText(pTime.getAsr());
//        tvMaghrib.setText(pTime.getMaghrib());
//        tvIsha.setText(pTime.getIsha());
//
//        // Return the completed view to render on screen
//        return convertView;
//    }


}
