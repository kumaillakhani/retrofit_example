package net.itlogics.jsonparser.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kumail Raza on 9/5/2016.
 */

public class RetroClient {

    private static final String ROOT_URL = "http://muslimsalat.com";

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static RetrofitArrayAPI getRetrofitApiService() {
        return getRetrofitInstance().create(RetrofitArrayAPI.class);
    }
}
