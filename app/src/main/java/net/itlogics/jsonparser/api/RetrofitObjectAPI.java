package net.itlogics.jsonparser.api;

import net.itlogics.jsonparser.model.PTime;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by Kumail Raza on 9/4/2016.
 */

public interface RetrofitObjectAPI {

    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
    */
    @GET("api/RetrofitAndroidObjectResponse")
    Call<PTime> getPTimes();
}
