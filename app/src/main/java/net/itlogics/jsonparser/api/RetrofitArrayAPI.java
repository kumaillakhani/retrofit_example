package net.itlogics.jsonparser.api;

import net.itlogics.jsonparser.model.PTime;
import net.itlogics.jsonparser.model.PTimeList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Kumail Raza on 9/4/2016.
 */

public interface RetrofitArrayAPI {
    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
    */
    @GET("london/weekly/true/5.json?key=d811f00eec0a0944c6d21a29d64c0927")
    Call<PTimeList> getServerPTimes();
}
