package net.itlogics.jsonparser.model;

import java.util.ArrayList;

/**
 * Created by Kumail Raza on 9/5/2016.
 */

public class PTimeList {

    private ArrayList<PTime> items = new ArrayList<>();

    public ArrayList<PTime> getItems() {
        return items;
    }

    public void setItems(ArrayList<PTime> items) {
        this.items = items;
    }

}
