package net.itlogics.jsonparser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static android.R.attr.name;

/**
 * Created by Kumail Raza on 9/4/2016.
 */

public class PTime {

    @SerializedName("date_for")
    @Expose
    String date_for;

    @SerializedName("fajr")
    @Expose
    String fajr;

    @SerializedName("shurooq")
    @Expose
    String shurooq;

    @SerializedName("dhuhr")
    @Expose
    String dhuhr;

    @SerializedName("asr")
    @Expose
    String asr;

    @SerializedName("maghrib")
    @Expose
    String maghrib;

    @SerializedName("isha")
    @Expose
    String isha;

//    String KEY_JSON_DATE = "date_for";
//    String KEY_JSON_FAJr = "fajr";
//    String KEY_JSON_SHUROOQ = "shurooq";
//    String KEY_JSON_DHUHR = "dhuhr";
//    String KEY_JSON_ASR = "asr";
//    String KEY_JSON_MAGHRIB = "maghrib";
//    String KEY_JSON_ISHA = "isha";

    public PTime(String date_for, String fajr, String shurooq, String dhuhr, String asr,
                 String maghrib, String isha) {
        this.date_for = date_for;
        this.fajr = fajr;
        this.shurooq = shurooq;
        this.dhuhr = dhuhr;
        this.asr = asr;
        this.maghrib = maghrib;
        this.isha = isha;
    }

    public String getDate_for() {
        return date_for;
    }

    public void setDate_for(String date_for) {
        this.date_for = date_for;
    }

    public String getFajr() {
        return fajr;
    }

    public void setFajr(String fajr) {
        this.fajr = fajr;
    }

    public String getShurooq() {
        return shurooq;
    }

    public void setShurooq(String shurooq) {
        this.shurooq = shurooq;
    }

    public String getDhuhr() {
        return dhuhr;
    }

    public void setDhuhr(String dhuhr) {
        this.dhuhr = dhuhr;
    }

    public String getAsr() {
        return asr;
    }

    public void setAsr(String asr) {
        this.asr = asr;
    }

    public String getMaghrib() {
        return maghrib;
    }

    public void setMaghrib(String maghrib) {
        this.maghrib = maghrib;
    }

    public String getIsha() {
        return isha;
    }

    public void setIsha(String isha) {
        this.isha = isha;
    }

}
