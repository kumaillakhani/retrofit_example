package net.itlogics.jsonparser;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import net.itlogics.jsonparser.adapter.PTimeAdapter;
import net.itlogics.jsonparser.api.RetrofitArrayAPI;
import net.itlogics.jsonparser.model.PTime;
import net.itlogics.jsonparser.model.PTimeList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static net.itlogics.jsonparser.api.RetroClient.getRetrofitApiService;

public class MainActivity extends AppCompatActivity {

    Context mContext = MainActivity.this;

    /**
     * Views
     */
    private ListView listView;
    private View parentView;

    private ArrayList<PTime> pTimeList;
    private PTimeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Array List for Binding Data from JSON to this List
         */
        pTimeList = new ArrayList<>();

        /**
         * Getting List and Setting List Adapter
         */
        listView = (ListView) findViewById(R.id.lvData);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(mContext, pTimeList.get(position).getDate_for() + " => " + pTimeList.get(position).getFajr(), Toast.LENGTH_LONG).show();
            }
        });

        getData();
    }

    public void getData() {

        /**
         * Checking Internet Connection
         */
        if (InternetConnection.checkConnection(getApplicationContext())) {
            final ProgressDialog dialog;
            /**
             * Progress Dialog for User Interaction
             */
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setTitle(getString(R.string.string_getting_json_title));
            dialog.setMessage(getString(R.string.string_getting_json_message));
            dialog.show();

            //Creating an object of our api interface
            RetrofitArrayAPI api = getRetrofitApiService();

            /**
             * Calling JSON
             */
            Call<PTimeList> call = api.getServerPTimes();

            /**
             * Enqueue Callback will be call when get response...
             */
            call.enqueue(new Callback<PTimeList>() {
                @Override
                public void onResponse(Call<PTimeList> call, Response<PTimeList> response) {
                    //Dismiss Dialog
                    dialog.dismiss();

                    if(response.isSuccessful()) {
                        /**
                         * Got Successfully
                         */
                        pTimeList = response.body().getItems();

                        /**
                         * Binding that List to Adapter
                         */
                        adapter = new PTimeAdapter(MainActivity.this, pTimeList);
                        listView.setAdapter(adapter);

                    } else {
                        Toast.makeText(mContext, R.string.string_some_thing_wrong, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<PTimeList> call, Throwable t) {
                    dialog.dismiss();
                }
            });

        } else {
            Toast.makeText(mContext, "Internet connection not available.", Toast.LENGTH_LONG).show();
        }

    }

}
